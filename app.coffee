http = require "http"
express = require "express"
app = express()
config = require "./config"
path = require "path"
passport = require "passport"
bp = require "body-parser"
bb = require "connect-busboy"
cp = require "cookie-parser"
sess = require "express-session"
Strategy = require('passport-http').BasicStrategy;
mongoose = require "mongoose"
  .connect config.db

passport.use new Strategy (user, pass, done) ->
  if process.env.env is "dev"
    done(null, "admin")
    return
  if user + ":" + pass is config.auth
    done(null, "admin")
  else
    done(null, false)

passport.serializeUser (user, done) ->
  done(null, "admin")
passport.deserializeUser (user, done) ->
  done(null, "admin")


allowCrossDomain = (req, res, next) ->
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Authorization');

  next();

app.use bp()
app.use bb()
app.use cp()
app.use sess({secret: "sss"})
app.use passport.initialize()
app.use passport.session()
app.use allowCrossDomain

app.set 'port', process.env.PORT || 4013
.set 'views', path.join(__dirname, './views')
  .set 'view engine', 'jade'
    .use express.static path.join __dirname, './public'

app.get "/", passport.authenticate('basic'), (req, res) ->
  res.render "index"

for path, route of config.routes.get
  console.log "get " + route
  app.get path, passport.authenticate('basic', { session: false }), require "./routes/" + route

for path, route of config.routes.post
  console.log "post " + route
  app.post path, passport.authenticate('basic', { session: false }), require "./routes/" + route

for path, route of config.routes.put
  console.log "put " + route
  app.put path, passport.authenticate('basic', { session: false }), require "./routes/" + route

for path, route of config.routes.delete
  console.log "delete " + route
  app.delete path, passport.authenticate('basic', { session: false }), require "./routes/" + route

server = http.createServer app
.listen app.get "port"

console.log app.get "port"