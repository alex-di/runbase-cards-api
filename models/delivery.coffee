mongoose = require 'mongoose'
Schema = mongoose.Schema
autoInc = require "mongoose-auto-increment"
autoInc.initialize mongoose

Delivery = new Schema
  from: Number
  to: Number
  status:
    type: Number
    default: 0
  id: Number
  card_number: Number


Delivery.plugin(autoInc.plugin, { model: 'Delivery', field: 'id' });

module.exports = mongoose.model "Delivery", Delivery