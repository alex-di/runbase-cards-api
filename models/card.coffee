mongoose = require 'mongoose'
moment = require 'moment'
Schema = mongoose.Schema
autoInc = require "mongoose-auto-increment"
autoInc.initialize mongoose

Card = new Schema
  card_id:
    type: Number
    index: true

  rfid_number:
    type: String

  number:
    type: Number

  card_status:
    type: Number
    default: 140

  date_create:
    type: Date
    default: Date.now

  date_activation:
    type: Date

  email:
    type: String

  phone:
    type: String

  ready:
    type: Boolean
    default: false

  runbase:
    type: String
    default: 0

Card.plugin(autoInc.plugin, { model: 'Card', field: 'card_id' });
model = mongoose.model "Card", Card
model.schema.path "email"
  .validate (value) ->
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test value
  , "Invalid email"
module.exports = model
