mongoose = require 'mongoose'
Schema = mongoose.Schema
autoInc = require "mongoose-auto-increment"
autoInc.initialize mongoose

Runbase = new Schema
  address: String
  name: String
  id: Number


Runbase.plugin(autoInc.plugin, { model: 'Runbase', field: 'id' });

module.exports = mongoose.model "Runbase", Runbase