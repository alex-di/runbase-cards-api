Backend = require "./backend"
Notification = require "./notification"

module.exports = (id) ->
  self = riot.observable @
  self.id = id
  tpl = $("#templates-request").html()
  console.log tpl
  properties = {}

  req = new Backend "/requests/" + self.id
  req.on "loaded", (data) ->
    self.$el = $("<tr></tr>", {class: "request-wrap"})
    $.extend properties, data
    self.$el.on "click", ".control-deny", ->
      self.$el.addClass "loading"
      deny = new Backend "/requests/" + self.id + "/deny", {type: "PUT"}
      deny.on "loaded", (request) ->
        if request.status is "ok"
          new Notification "normal", "Заявка отклонена"
          self.$el.slideUp().remove()
        else
          new Notification "error", "Произошла ошибка. Заявка не отклонена. А может и отклонена. Это же ошибка. Я не могу знать наверняка."

        self.$el.removeClass "loading"
      return false

    self.$el.on "click", ".control-approve", ->
      self.$el.addClass "loading"
      deny = new Backend "/requests/" + self.id + "/approve", {type: "PUT"}
      deny.on "loaded", (request) ->
        if request.status is "ok"
          new Notification "normal", "Заявка одобрена"
          self.$el.slideUp().remove()
        else
          new Notification "error", "Произошла ошибка. Заявка не одобрена. А может и одобрена. Это же ошибка. Я не могу знать наверняка."
          console.log request.error


        self.$el.removeClass "loading"
      return false

    self.render ->
      self.trigger "ready"

  self.render = (cb) ->

    b = new Backend "/runbases"
    b.on "loaded", (runbases) ->
      for runbase in runbases
        properties.runbaseName = runbase.name if runbase.id is properties.runbase
      self.$el.html(riot.render tpl, properties)
      cb() if cb?


  self.get = (prop) ->
    return properties[prop]

  self.set = (prop, value) ->
    unless properties[prop]? and properties[prop] is value
      properties[prop] = value
      self.trigger "update", prop, value

  self.on "update", ->
    console.log properties
    self.render()

  return self