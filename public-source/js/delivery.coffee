Backend = require "./backend"

Delivery = (id) ->
  self = riot.observable(@)
  tpl = $("#templates-delivery").html()
  properties = {}

  self.on "ready", (data) ->
    self.$el = $("<tr></tr>", {class: "delivery-wrap"})
    $.extend properties, data
    self.render()

  self.render = (cb) ->
    properties.from_name = document.runbases[properties.from].name
    properties.to_name = document.runbases[properties.to].name
    self.$el.html(riot.render tpl, properties)

    cb() if cb?


  self.get = (prop) ->
    return properties[prop]

  self.set = (prop, value) ->
    unless properties[prop]? and properties[prop] is value
      properties[prop] = value
      self.trigger "update", prop, value

  self.on "update", ->
    console.log properties
    self.render()

  request = new Backend "/deliveries/" + id
  request.on "loaded", (data) ->
    console.log data
    self.trigger "ready", data

  return self

module.exports = Delivery
