// Generated by CoffeeScript 1.7.1
var Backend, Card, cardStatuses;

Backend = require("./backend");

cardStatuses = {
  "140": "Не привязана, не активирована",
  "141": "Привязана, не активирована",
  "142": "Активна и привязана"
};

Card = function(id) {
  var properties, request, self, tpl;
  self = riot.observable(this);
  tpl = $("#templates-card").html();
  properties = {};
  self.on("ready", function(data) {
    self.$el = $("<tr></tr>", {
      "class": "card-wrap"
    });
    $.extend(properties, data);
    return self.render();
  });
  self.render = function(cb) {
    self.$el.html(riot.render(tpl, properties));
    if (cb != null) {
      return cb();
    }
  };
  self.get = function(prop) {
    return properties[prop];
  };
  self.set = function(prop, value) {
    if (!((properties[prop] != null) && properties[prop] === value)) {
      properties[prop] = value;
      return self.trigger("update", prop, value);
    }
  };
  self.on("update", function() {
    console.log(properties);
    return self.render();
  });
  request = new Backend("/cards/" + id);
  request.on("loaded", function(data) {
    return self.trigger("ready", data);
  });
  return self;
};

module.exports = Card;
