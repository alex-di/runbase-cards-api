Backend = require "./backend"
Card = require "./card"
Request = require "./request"
Notification = require "./notification"
Delivery = require "./delivery"

showActivePath = (slice) ->
  console.log slice
  $("#menu a[href='#/" + slice + "']").parent().addClass "active"
  .parents("li").addClass "active"


getRunbaseList = (cb) ->
  b = new Backend "/runbases"
  b.on "loaded", (runbases) ->
    rList = for runbase in runbases
      "<option value='" + runbase.id + "'>" + runbase.name + " (id:" + runbase.id + ")</option>"
    cb rList.join ""

app (data) ->
  if location.hash is ""
    location.hash = "/requests"
  $("body").on "click", "a[href^='#/']", ->
    riot.route($(this).attr("href"));

  riot.route (path) ->
    slice = path.slice path.indexOf("#/") + 2
    $("#menu li").removeClass("active")

    reg = /(cards|requests|cards\/deliveries)\/([0-9]+)/
    if reg.test slice
      page = (slice.replace reg, "$2") * 1
      slice = slice.replace reg, "$1"
    showActivePath slice

    data.root.html ""
    switch slice
      when "cards", "cards/archive"
        page = 0 unless page?

        cardsRequest = new Backend "/cards", {data: {limit: 25, skip: 25 * page, params: (unless slice is "cards/archive" then {card_status: 140} else {card_status: {$ne: 140}})}}
        cardsRequest.on "loaded", (cardsData) ->
          if cardsData.length is 0
            new Notification "warning", "Нет карт по запросу"
          else
            rootW = $(riot.render $("#templates-cards-wrapper").html(), {pagination:
              page: page
              prev: (if page is 0 then "" else "<a href='#/cards/" + (page - 1) + "')><< prev</a>")
              next: (if cardsData.length < 25 then "" else "<a href='#/cards/" + (page + 1) + "'>next >></a>")
            })
            rootW.appendTo data.root
            root = rootW.find(".data-wrapper")
            for item in cardsData
              (new Card item.card_id).on "ready", ->
                root.append this.$el
            data.root.removeClass "loading"

      when "requests"
        page = 0 unless page?
        rRequest = new Backend "/requests", {data: {limit: 25, skip: 25 * page, params: {status: {$in: [0, null]}}}}
        rRequest.on "loaded", (rData) ->
          if rData.length is 0
            new Notification "warning", "Нет таких запросов"
          else
            rootW = $(riot.render $("#templates-requests-wrapper").html(), {pagination:
              page: page
              prev: (if page is 0 then "" else "<a href='#/requests/" + (page - 1) + "')><< prev</a>")
              next: (if rData.length < 25 then "" else "<a href='#/requests/" + (page + 1) + "'>next >></a>")
            })
            rootW.appendTo data.root
            root = rootW.find(".data-wrapper")
            for item in rData
              (new Request item.id).on "ready", ->
                root.append this.$el
          data.root.removeClass "loading"

      when "requests/add"
        root = $(riot.render $("#templates-add-wrapper").html())
        root.appendTo data.root

        root.on "submit", "form.file", (e) ->
          file = $(this).find("input[type='file']")[0].files[0]

          unless file?
            new Notification "error", "Нет файла"
            return false

          unless file.type is "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            new Notification "error", "Поддерживается только формат таблицы Excel - xlsx. Если ваш формат совпадает в требуемым - обратитесь к администратору"
            return false

          fData = new FormData()
          fData.append "table", file, file.name

          xhr = new XMLHttpRequest()
          xhr.open "POST", "/requests/upload", true
          xhr.onload = ->
            if xhr.status is 200
              new Notification "normal", "Заявки загружены"
              $("form.file").find("input[type='file']").val("")
            else
              new Notification "error", "Что-то пошло не так. Заявки не загрузились =("
              console.log xhr

          xhr.send fData

          e.preventDefault()
          return false


        addField = ->
          getRunbaseList (runbases) ->
            root.find(".fields-wr").append riot.render $("#templates-requests-add-field").html(), {runbaseList: runbases}

        addField()

        root.on "click", ".addField", addField

        root.on "submit", "form.fields", ->
          form = $(this).formParams()
          if typeof form.name is "string"
            fData = [form]
          else
            fData = for i in [0..(form.name.length - 1)]
              {name: form.name[i]
                email: form.email[i]
                phone: form.phone[i]
                runbase: form.runbase[i]}

          request = new Backend "/requests/manual", {data: {requests: fData}, type: "post" }
          request.on "loaded", (data) ->
            if data.status is "ok"
              new Notification "normal", "Заявки загружены"
              root.find(".fields-wr").html("")
              addField()

            else
              new Notification "error", "Что-то пошло не так. Заявки не загрузились =("


          return false

      when "cards/add"
        root = $(riot.render $("#templates-add-wrapper").html())
        root.appendTo data.root

        root.on "submit", "form.file", (e) ->
          file = $(this).find("input[type='file']")[0].files[0]

          unless file?
            new Notification "error", "Нет файла"
            return false

          unless file.type is "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            new Notification "error", "Поддерживается только формат таблицы Excel - xlsx. Если ваш формат совпадает в требуемым - обратитесь к администратору"
            return false

          fData = new FormData()
          fData.append "table", file, file.name

          xhr = new XMLHttpRequest()
          xhr.open "POST", "/cards/upload", true
          xhr.onload = ->
            if xhr.status is 200
              new Notification "normal", "Карты загружены"
              $("form.file").find("input[type='file']").val("")
            else
              new Notification "error", "Что-то пошло не так. Карты не загрузились =("
              console.log xhr

          xhr.send fData

          e.preventDefault()
          return false


        addField = ->
          getRunbaseList (runbases) ->
            root.find(".fields-wr").append riot.render $("#templates-cards-add-field").html(), {runbaseList: runbases}

        addField()

        root.on "click", ".addField", addField

        root.on "submit", "form.fields", ->
          form = $(this).formParams()
          if typeof form.name is "string"
            fData = [form]
          else
            fData = for i in [0..(form.name.length - 1)]
              {number: form.number[i]
                rfid_number: form.rfid_number[i]
                runbase: form.runbase[i]}

          request = new Backend "/cards/manual", {data: {cards: fData}, type: "post" }
          request.on "loaded", (data) ->
            if data.status is "ok"
              new Notification "normal", "Карты загружены"
              root.find(".fields-wr").html("")
              addField()

            else
              new Notification "error", "Что-то пошло не так. Карты не загрузились =("


          return false


      when "cards/deliveries"
        page = 0 unless page?

        r = new Backend "/deliveries", {data: {limit: 25, skip: 25 * page }}
        r.on "loaded", (dData) ->
          if dData.length is 0
            new Notification "warning", "Нет ожидающих доставки карт"
          else
            rootW = $(riot.render $("#templates-deliveries-wrapper").html(), {pagination:
              page: page
              prev: (if page is 0 then "" else "<a href='#/deliveries/" + (page - 1) + "')><< prev</a>")
              next: (if dData.length < 25 then "" else "<a href='#/deliveries/" + (page + 1) + "'>next >></a>")
            })
            rootW.appendTo data.root
            root = rootW.find(".data-wrapper")

            b = new Backend "/runbases"
            b.on "loaded", (runbases) ->
              document.runbases = runbases
              for item in dData
                (new Delivery item.id).on "ready", ->
                  root.append this.$el
              data.root.removeClass "loading"


      when "cards/bind"
        root = $(riot.render $("#templates-bind-wrapper").html())
        root.appendTo data.root

        root.on "submit", "form.bind", ->
          form = $(@)
          formData = form.formParams()


          if !formData.email? || !formData.email || formData.email is ""
            new Notification "warning", "Нужно ввести email"
            return false


          check = new Backend "/cards/bynum/" + formData.number

          check.on "loaded", (card) ->
            console.log card
            if card.card_status != 140 or (card.email? && card.email)
              new Notification "error", "Кажется карта уже привязана"
            else
              req = new Backend "/cards/" + card.card_id, {type: "put", data: formData}
              req.on "loaded", (resp) ->
                form.find("input[type='text']").val("")
                new Notification "normal", "Карта привязана"

          check.on "error", (error) ->
            if error.status is 404 then new Notification "error", "Нет карты с таким номером" else new Notification "error", "Что-то пошло совсем не так, срочно свяжись с разработчиком и накричи на него!"
#
          return false

      else
        new Notification "warning", "Не найдено такой страницы"

  riot.route location.href

owner = app {
  root: $("#data-wrapper")
}