Backend = require "./backend"

cardStatuses =
  "140": "Не привязана, не активирована"
  "141": "Привязана, не активирована"
  "142": "Активна и привязана"

Card = (id) ->
  self = riot.observable(@)
  tpl = $("#templates-card").html()
  properties = {}

  self.on "ready", (data) ->
    self.$el = $("<tr></tr>", {class: "card-wrap"})
    $.extend properties, data
    self.render()

  self.render = (cb) ->
    self.$el.html(riot.render tpl, properties)

    cb() if cb?


  self.get = (prop) ->
    return properties[prop]

  self.set = (prop, value) ->
    unless properties[prop]? and properties[prop] is value
      properties[prop] = value
      self.trigger "update", prop, value

  self.on "update", ->
    console.log properties
    self.render()

  request = new Backend "/cards/" + id
  request.on "loaded", (data) ->
    self.trigger "ready", data

  return self

module.exports = Card
