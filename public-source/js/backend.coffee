Backend = (url, prop) ->
  self = riot.observable(@)
  $.ajax url, prop
  .done (res) ->
      self.trigger "loaded", res
      if conf.env is "dev"
        console.log url, "loaded"

  .fail (request, textStatus, error) ->
      console.log url, error
      self.trigger "error", request

  return self

module.exports = Backend