module.exports = (type, text) ->
#  alert riot.render "{type}: {text}", {type: type, text: text}
  switch type
    when "error"
      opts =
        globalPosition: "top left"
        autoHide: false
        className: "error"
    when "warning"
      opts =
        globalPosition: "top left"
        className: "warning"
    else
      opts =
        globalPosition: "top left"
        className:  "success"

  $.notify text, opts