config = require "../config"
should = require "should"
request = require "superagent"
mongoose = require "mongoose"
.connect config.db
Card = require "../models/card"

suite "Test web-server is up and responsible", ->
  addr = "localhost:4013"
  auth = config.auth


  beforeEach (done)->
    Card.remove {}, (err) ->
      throw err if err
      Card.resetCount (err, next) ->
        throw err if err
        card = new Card
          id: 0
          number: 1000000
          rfid_number: "[0400](130,123123)"

        card.save (err, card) ->
          throw err if err
          done()

  suite "Accessing pages without auth should response 401", ->
    test "index", (done) ->
      request.get addr
      .end (res) ->
          res.status.should.eql 401
          done()
    test "get cards", (done) ->
      request.get addr + "/cards"
      .end (res) ->
          res.status.should.eql 401
          done()
    test "post card", (done) ->
      request.post addr + "/cards"
      .end (res) ->
          res.status.should.eql 401
          done()

  test "Accessing pages with auth should have response code 200 and json", (done) ->
    request.get addr
    .set "Accept", "application/json"
      .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
        .end (res) ->
            res.status.should.eql 200
            res.text.should.be.json
            done()

  suite "Correct pages response", ->
    test "Index page should contain html header", (done) ->
      request.get addr
      .set "Accept", "application/json"
        .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
          .end (res) ->
              res.text.should.be.html
              res.text.should.containEql "Adidas-running card api"
              done()

    suite "Cards page should return cards list", ->
      test "Should return json array by default", (done) ->
        request.get addr + "/cards"
        .set "Accept", "application/json"
          .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
            .end (res) ->
                res.status.should.eql 200
                res.text.should.be.json

                data = JSON.parse(res.text)
                data.items.should.be.an.instanceOf(Array);

                data.items[0].should.have.properties ["card_id", "rfid_number"]

                done()

      test "Should return xml if client request it", (done) ->
        request.get addr + "/cards"
        .send
          xml: true
        .set "Accept", "application/xml"
          .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
            .end (res) ->
                res.status.should.eql 200
                res.text.should.be.xml
                console.log res.text
                done()




  suite "Adding items", ->
    test "Add new card should response json with status and ID of the added item", (done) ->
      request.post addr + '/cards'
      .send
          rfid_number: "[0400](130,48782)"
          card_id: 10000001
      .set "Accept", "application/json"
        .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
          .end (res) ->
              res.should.be.json
              data = JSON.parse res.text
              data.should.be.type "object"
              data.should.have.property "status", "Ok"
              data.should.exists
              data.should.be.type "object"
              data.should.have.property "card_id"
              done()

  suite "Updating items", ->
    test "Updating card should return json with status and ID of the updated card", (done) ->
      request.put addr + "/cards/0"
      .send
          rfid_number: "[0400](130,123123)"
      .set "Accept", "application/json"
        .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
          .end (res) ->
              res.should.be.json
              data = JSON.parse res.text
              data.should.be.type "object"
              data.should.have.property "status", "ok"
              data.should.have.property "card_id", 0
              request.get "/cards/0"
              .set "Accept", "application/json"
                .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
                  .end (res) ->
                      res.should.be.json
                      data = JSON.parse res.text
                      data.should.be.type "object"
                      data.should.have.property "status", "Ok"
                      data.should.have.property "card_id", 0
                      data.item.should.exists
                      data.item.should.be.type "object"
                      data.item.should.have.property "rfid_number", "[0400](130,123123)"
                      done()

  suite "Deleting items", ->
    test "Deleting card should return json with status `Ok`", (done) ->
      request.post addr + '/cards'
      .send
          rfid_number: "[0400](130,48782)"
          card_id: 10000001
      .set "Accept", "application/json"
        .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
          .end (res) ->
              res.should.be.json
              data = JSON.parse res.text
              data.should.be.type "object"
              data.should.have.property "status", "Ok"
              data.should.have.property "card_id"

              request.del addr + "/cards/" + data.card_id
              .set "Accept", "application/json"
                .set "Authorization", "Basic " + new Buffer(auth).toString("base64")
                  .end (res) ->
                      res.should.be.json
                      data = JSON.parse res.text
                      data.should.have.property "status", "Ok"

                      done()