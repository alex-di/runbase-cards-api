Delivery = require "../models/delivery"

module.exports = (req, res) ->
  filter = req.params.id
  if filter.indexOf(",") > -1
    # TODO: Explode filter
    filter = kindaExplode filter, ","
    Delivery.find {id: {$in: filter}}, {_id: 0, __v: 0}, (err, request) ->
      return res.json {status: "Error", error: err} if err
      res.json request

  else
    Delivery.findOne {id: filter}, {_id: 0, __v: 0}, (err, request) ->
      return res.json {status: "Error", error: err} if err
      res.json request
