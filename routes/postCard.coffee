Card = require "../models/card"

module.exports = (req, res) ->
  card = new Card req.body
  card.save (err) ->
    data =
      status: "Ok"
      card_id: card.card_id
    return res.json data  unless err

    console.log err
    res.json
      status: "Error"
      error: err