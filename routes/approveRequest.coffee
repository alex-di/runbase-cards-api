Request = require "../models/request"
Card = require "../models/card"
Delivery = require "../models/delivery"

module.exports = (req, res) ->
  id = req.params.id
  Request.findOne {id: id}, (err, request) ->
    if err
      console.log err
      res.json {status: "error", error: err}
      return
    unless request?
      console.log "No request with id " + id
      return

    request.status = 2
    console.log request

    Card.findOne {card_status: 140, runbase: request.runbase}, (err, card) ->
      if err
        console.log err
        res.json {status: "Error", error: err}
        return

      if card?
        card.status = 142
        card.phone = request.phone
        card.email = request.email
        card.date_activation = Date.now()
        card.save ->
          request.card = card.card_id
          request.save (err) ->
            if err
              console.log err
              res.json {status: "Error", error: err}
              return
            res.json {status: "ok"}

      else
        Card.findOne {card_status: 140}, (err, card) ->
          if err
            console.log err
            res.json {status: "Error", error: err}
            return
          unless card?
            res.json {status: "Error", error: "No free cards"}
            return

          card.status = 142
          card.phone = request.phone
          card.email = request.email
          card.date_activation = Date.now()
          card.save ->
            request.card = card.card_id
            request.save (err) ->
              if err
                console.log err
                res.json {status: "Error", error: err}
                return
              delivery = new Delivery
                from: card.runbase
                to: request.runbase
                card_number: card.number
              delivery.save (err) ->
                if err
                  console.log err
                  res.json {status: "Error", error: err}
                  return
                res.json {status: "ok"}


#    request.status = 2
#    request.save (err) ->
#      if err
#        console.log err
#        res.json {status: "error", error: err}
#        return
#
#      Card.findOne {status: 140}, (err, card) ->
#        if err
#          console.log err
#          res.json {status: "Error", error: err}
#        return
#
#        card.status = 142
#        card.phone = request.phone
#        card.email = request.email
#        card.date_activation = Date.now()
#        card.save ->
#
#          request.card = card.card_id
#          request.save ->
#            res.json {status: "ok"}
