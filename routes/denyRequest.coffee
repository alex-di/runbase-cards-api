Request = require "../models/request"

module.exports = (req, res) ->
  id = req.params.id
  Request.findOne {id: id}, (err, request) ->
    if err
      console.log err
      res.json {status: "error", error: err}
      return
    unless request?
      console.log "No request with id " + id
      return
    request.status = 1
    request.save (err) ->
      if err
        console.log err
        res.json {status: "error", error: err}
        return

      res.json {status: "ok"}