Card = require "../models/card"

module.exports = (req, res, next) ->
  Card.findOne {email: req.params.email}, (err, card) ->
    if card?
      res.json card
    else
      res.send 404
