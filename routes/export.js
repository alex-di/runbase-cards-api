// Generated by CoffeeScript 1.7.1
var Card, moment,
  __hasProp = {}.hasOwnProperty;

Card = require("../models/card");

moment = require("moment");

module.exports = function(req, res) {
  var format;
  format = req.params.format;
  return Card.find({}, function(err, cards) {
    var card, fParams, item, items, key, params, value, xmlOut, _i, _len;
    if (err) {
      console.log(err);
      res.json({
        error: err
      });
      return;
    }
    switch (format) {
      case "xml":
        format = "YYYY-MM-DD HH:mm:ss";
        items = ["<AdidasRFID ver='1.0'><Response asof='" + moment().format(format) + "'><Cards>"];
        for (_i = 0, _len = cards.length; _i < _len; _i++) {
          card = cards[_i];
          item = '<Card ';
          params = {};
          params.id = card.card_id;
          params.rfid_number = card.rfid_number || '';
          params.card_status = card.card_status || 140;
          params.number = card.number || 0;
          params.sex = card.sex || "";
          params.date_create = moment(card.date_create, "dd MMM DD YYYY HH:mm:ss [GMT]ZZ [(MSK)]").format(format);
          params.date_activation = card.date_activation != null ? moment(card.date_activation, "dd MMM DD YYYY HH:mm:ss [GMT]ZZ [(MSK)]").format(format) : "";
          fParams = [];
          for (key in params) {
            if (!__hasProp.call(params, key)) continue;
            value = params[key];
            fParams.push(key + '="' + value + '"');
          }
          item += fParams.join(" ") + "/>";
          items.push(item);
        }
        items.push("</Cards></Response></AdidasRFID>");
        xmlOut = items.join("");
        return res.header('Content-Type', 'text/xml').send(xmlOut);
      default:
        return res.json({
          error: "unknown format"
        });
    }
  });
};
