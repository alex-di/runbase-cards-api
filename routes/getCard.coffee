Card = require "../models/card"

module.exports = (req, res) ->
  filter = req.params.id
  if filter.indexOf(",") > -1
    # TODO: Explode string
    filter = kindaExplode filter, ","
    Card.find {card_id: {$in: filter}} # Filter must be an array
    , (err, cards) ->
      if cards.length > 0
        res.json cards
      else
        res.send 404
  else
    Card.findOne {card_id: filter}, (err, card) ->
      if card?
        res.json card
      else
        res.send 404