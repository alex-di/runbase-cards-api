// Generated by CoffeeScript 1.7.1
var Delivery;

Delivery = require("../models/delivery");

module.exports = function(req, res) {
  return Delivery.find(req.query.params, "id card_number", function(err, d) {
    if (err) {
      console.log(err);
      res.json({
        error: err
      });
      return;
    }
    return res.json(d);
  });
};
