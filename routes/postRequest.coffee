Request = require "../models/request"

module.exports = (req, res) ->
  request = new Request req.body
  request.save (err) ->
    data =
      status: "Ok"
      id: request.id
    return res.json data  unless err

    console.log err
    res.json
      status: "Error"
      error: err