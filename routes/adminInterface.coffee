Runbase = require "../models/runbase"

module.exports = (req, res) ->
  config = {}
  if process.env.env is "dev"
    config.env = "dev"

  Runbase.find {}, (err, runbases) ->

    res.render "ui",
      config: JSON.stringify config
      cardStatuses: [
        {
          name: "Не привязана, не активирована"
          key: 140
        },
        {
          name: "Привязана, не активирована"
          key: 141
        },
        {
          name: "Активна и привязана"
          key: 142
        }
      ]
      runbases: JSON.stringify runbases