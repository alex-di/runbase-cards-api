// Generated by CoffeeScript 1.7.1
var Request, xls;

xls = require("xlsx");

Request = require("../models/request");

module.exports = function(req, res) {
  req.pipe(req.busboy);
  return req.busboy.on('file', function(fieldname, file, filename) {
    var b;
    b = false;
    file.on("data", function(data) {
      if (b) {
        return b = Buffer.concat([b, data]);
      } else {
        return b = data;
      }
    });
    return file.on("end", function() {
      var data, obg, request, _i, _len;
      obg = xls.read(b);
      data = xls.utils.sheet_to_json(obg.Sheets[obg.Workbook.Sheets[0].name]);
      for (_i = 0, _len = data.length; _i < _len; _i++) {
        request = data[_i];
        (new Request(request)).save();
      }
      return res.send(200);
    });
  });
};
