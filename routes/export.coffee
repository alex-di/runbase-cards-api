Card = require "../models/card"
moment = require "moment"

module.exports = (req, res) ->
  format = req.params.format
  Card.find {}, (err, cards) ->
    if err
      console.log err
      res.json {error: err}
      return;

    switch format
      when "xml"
        format = "YYYY-MM-DD HH:mm:ss"
        items = ["<AdidasRFID ver='1.0'><Response asof='" + moment().format(format) + "'><Cards>"]
        for card in cards
          item = '<Card '
          params = {}
          params.id = card.card_id
          params.rfid_number = card.rfid_number || ''
          params.card_status = card.card_status || 140
          params.number = card.number || 0
          params.sex = card.sex || ""
          params.date_create = moment(card.date_create,
            "dd MMM DD YYYY HH:mm:ss [GMT]ZZ [(MSK)]").format(format)
          params.date_activation = if card.date_activation? then moment(card.date_activation, "dd MMM DD YYYY HH:mm:ss [GMT]ZZ [(MSK)]").format(format) else ""
          fParams = []
          for own key, value of params
            fParams.push key + '="' + value + '"'
          item += fParams.join(" ") + "/>"
          items.push item
        items.push "</Cards></Response></AdidasRFID>"
        xmlOut = items.join ""

        res.header 'Content-Type', 'text/xml'
        .send xmlOut

      else
        res.json {error: "unknown format"}