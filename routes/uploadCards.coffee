xls = require "xlsx"
Card = require "../models/card"

module.exports = (req, res) ->
  req.pipe req.busboy
  req.busboy.on 'file', (fieldname, file, filename) ->
    b = false
    file.on "data", (data) ->
      if b
        b = Buffer.concat [ b, data]
      else
        b = data
    file.on "end", ->
      obg = xls.read b
      data = xls.utils.sheet_to_json obg.Sheets[obg.Workbook.Sheets[0].name]

      for request in data
        (new Card request).save()

      res.send 200