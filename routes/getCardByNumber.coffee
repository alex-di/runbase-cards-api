Card = require "../models/card"

module.exports = (req, res, next) ->
  Card.findOne {number: req.params.num}, (err, card) ->
    if card?
      res.json card
    else
      res.send 404
