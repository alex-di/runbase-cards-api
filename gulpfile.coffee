gulp = require "gulp"
browserify = require "gulp-browserify"
watch = require "gulp-watch"
plumber = require "gulp-plumber"

gulp.task "default", ->
  watch {glob: "public-source/js/*.js"},  ->
    gulp.src "public-source/js/*.js"
    .pipe plumber()
      .pipe browserify
          insertGoals: true
          debug: true


        .pipe gulp.dest "./public/js"

  watch {glob: "public-source/css/*.css"},  ->
    gulp.src "public-source/css/*.css"
    .pipe plumber()
      .pipe browserify
          insertGoals: true
          debug: true


        .pipe gulp.dest "./public/js"
