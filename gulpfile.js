// Generated by CoffeeScript 1.7.1
var browserify, gulp, plumber, watch;

gulp = require("gulp");

browserify = require("gulp-browserify");

watch = require("gulp-watch");

plumber = require("gulp-plumber");

gulp.task("default", function() {
  watch({
    glob: "public-source/js/*.js"
  }, function() {
    return gulp.src("public-source/js/*.js").pipe(plumber()).pipe(browserify({
      insertGoals: true,
      debug: true
    })).pipe(gulp.dest("./public/js"));
  });
  return watch({
    glob: "public-source/css/*.css"
  }, function() {
    return gulp.src("public-source/css/*.css").pipe(plumber()).pipe(browserify({
      insertGoals: true,
      debug: true
    })).pipe(gulp.dest("./public/js"));
  });
});
