(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// Generated by CoffeeScript 1.7.1
var Backend;

Backend = function(url, prop) {
  var self;
  self = riot.observable(this);
  $.ajax(url, prop).done(function(res) {
    self.trigger("loaded", res);
    if (conf.env === "dev") {
      return console.log(url, "loaded");
    }
  }).fail(function(request, textStatus, error) {
    console.log(url, error);
    return self.trigger("error", request);
  });
  return self;
};

module.exports = Backend;

},{}],2:[function(require,module,exports){
// Generated by CoffeeScript 1.7.1
var Backend, Card, cardStatuses;

Backend = require("./backend");

cardStatuses = {
  "140": "Не привязана, не активирована",
  "141": "Привязана, не активирована",
  "142": "Активна и привязана"
};

Card = function(id) {
  var properties, request, self, tpl;
  self = riot.observable(this);
  tpl = $("#templates-card").html();
  properties = {};
  self.on("ready", function(data) {
    self.$el = $("<tr></tr>", {
      "class": "card-wrap"
    });
    $.extend(properties, data);
    return self.render();
  });
  self.render = function(cb) {
    self.$el.html(riot.render(tpl, properties));
    if (cb != null) {
      return cb();
    }
  };
  self.get = function(prop) {
    return properties[prop];
  };
  self.set = function(prop, value) {
    if (!((properties[prop] != null) && properties[prop] === value)) {
      properties[prop] = value;
      return self.trigger("update", prop, value);
    }
  };
  self.on("update", function() {
    console.log(properties);
    return self.render();
  });
  request = new Backend("/cards/" + id);
  request.on("loaded", function(data) {
    return self.trigger("ready", data);
  });
  return self;
};

module.exports = Card;

},{"./backend":1}]},{},[2])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbGVrc2VqL3dvcmstcHJvamVjdHMvYWRpZGFzLWNhcmQtYXBpL25vZGVfbW9kdWxlcy9ndWxwLWJyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyIsIi9Vc2Vycy9hbGVrc2VqL3dvcmstcHJvamVjdHMvYWRpZGFzLWNhcmQtYXBpL3B1YmxpYy1zb3VyY2UvanMvYmFja2VuZC5qcyIsIi9Vc2Vycy9hbGVrc2VqL3dvcmstcHJvamVjdHMvYWRpZGFzLWNhcmQtYXBpL3B1YmxpYy1zb3VyY2UvanMvZmFrZV9jNmMyZDVjYy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpfXZhciBmPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChmLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGYsZi5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvLyBHZW5lcmF0ZWQgYnkgQ29mZmVlU2NyaXB0IDEuNy4xXG52YXIgQmFja2VuZDtcblxuQmFja2VuZCA9IGZ1bmN0aW9uKHVybCwgcHJvcCkge1xuICB2YXIgc2VsZjtcbiAgc2VsZiA9IHJpb3Qub2JzZXJ2YWJsZSh0aGlzKTtcbiAgJC5hamF4KHVybCwgcHJvcCkuZG9uZShmdW5jdGlvbihyZXMpIHtcbiAgICBzZWxmLnRyaWdnZXIoXCJsb2FkZWRcIiwgcmVzKTtcbiAgICBpZiAoY29uZi5lbnYgPT09IFwiZGV2XCIpIHtcbiAgICAgIHJldHVybiBjb25zb2xlLmxvZyh1cmwsIFwibG9hZGVkXCIpO1xuICAgIH1cbiAgfSkuZmFpbChmdW5jdGlvbihyZXF1ZXN0LCB0ZXh0U3RhdHVzLCBlcnJvcikge1xuICAgIGNvbnNvbGUubG9nKHVybCwgZXJyb3IpO1xuICAgIHJldHVybiBzZWxmLnRyaWdnZXIoXCJlcnJvclwiLCByZXF1ZXN0KTtcbiAgfSk7XG4gIHJldHVybiBzZWxmO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBCYWNrZW5kO1xuIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjcuMVxudmFyIEJhY2tlbmQsIENhcmQsIGNhcmRTdGF0dXNlcztcblxuQmFja2VuZCA9IHJlcXVpcmUoXCIuL2JhY2tlbmRcIik7XG5cbmNhcmRTdGF0dXNlcyA9IHtcbiAgXCIxNDBcIjogXCLQndC1INC/0YDQuNCy0Y/Qt9Cw0L3QsCwg0L3QtSDQsNC60YLQuNCy0LjRgNC+0LLQsNC90LBcIixcbiAgXCIxNDFcIjogXCLQn9GA0LjQstGP0LfQsNC90LAsINC90LUg0LDQutGC0LjQstC40YDQvtCy0LDQvdCwXCIsXG4gIFwiMTQyXCI6IFwi0JDQutGC0LjQstC90LAg0Lgg0L/RgNC40LLRj9C30LDQvdCwXCJcbn07XG5cbkNhcmQgPSBmdW5jdGlvbihpZCkge1xuICB2YXIgcHJvcGVydGllcywgcmVxdWVzdCwgc2VsZiwgdHBsO1xuICBzZWxmID0gcmlvdC5vYnNlcnZhYmxlKHRoaXMpO1xuICB0cGwgPSAkKFwiI3RlbXBsYXRlcy1jYXJkXCIpLmh0bWwoKTtcbiAgcHJvcGVydGllcyA9IHt9O1xuICBzZWxmLm9uKFwicmVhZHlcIiwgZnVuY3Rpb24oZGF0YSkge1xuICAgIHNlbGYuJGVsID0gJChcIjx0cj48L3RyPlwiLCB7XG4gICAgICBcImNsYXNzXCI6IFwiY2FyZC13cmFwXCJcbiAgICB9KTtcbiAgICAkLmV4dGVuZChwcm9wZXJ0aWVzLCBkYXRhKTtcbiAgICByZXR1cm4gc2VsZi5yZW5kZXIoKTtcbiAgfSk7XG4gIHNlbGYucmVuZGVyID0gZnVuY3Rpb24oY2IpIHtcbiAgICBzZWxmLiRlbC5odG1sKHJpb3QucmVuZGVyKHRwbCwgcHJvcGVydGllcykpO1xuICAgIGlmIChjYiAhPSBudWxsKSB7XG4gICAgICByZXR1cm4gY2IoKTtcbiAgICB9XG4gIH07XG4gIHNlbGYuZ2V0ID0gZnVuY3Rpb24ocHJvcCkge1xuICAgIHJldHVybiBwcm9wZXJ0aWVzW3Byb3BdO1xuICB9O1xuICBzZWxmLnNldCA9IGZ1bmN0aW9uKHByb3AsIHZhbHVlKSB7XG4gICAgaWYgKCEoKHByb3BlcnRpZXNbcHJvcF0gIT0gbnVsbCkgJiYgcHJvcGVydGllc1twcm9wXSA9PT0gdmFsdWUpKSB7XG4gICAgICBwcm9wZXJ0aWVzW3Byb3BdID0gdmFsdWU7XG4gICAgICByZXR1cm4gc2VsZi50cmlnZ2VyKFwidXBkYXRlXCIsIHByb3AsIHZhbHVlKTtcbiAgICB9XG4gIH07XG4gIHNlbGYub24oXCJ1cGRhdGVcIiwgZnVuY3Rpb24oKSB7XG4gICAgY29uc29sZS5sb2cocHJvcGVydGllcyk7XG4gICAgcmV0dXJuIHNlbGYucmVuZGVyKCk7XG4gIH0pO1xuICByZXF1ZXN0ID0gbmV3IEJhY2tlbmQoXCIvY2FyZHMvXCIgKyBpZCk7XG4gIHJlcXVlc3Qub24oXCJsb2FkZWRcIiwgZnVuY3Rpb24oZGF0YSkge1xuICAgIHJldHVybiBzZWxmLnRyaWdnZXIoXCJyZWFkeVwiLCBkYXRhKTtcbiAgfSk7XG4gIHJldHVybiBzZWxmO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBDYXJkO1xuIl19
